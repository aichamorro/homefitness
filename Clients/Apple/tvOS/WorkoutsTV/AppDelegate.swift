//
//  AppDelegate.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 02/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

func protocolName<P>(_ proto: P.Type) -> String {
    return "\(proto)"
}

func className<P>(_ clazz: P.Type) -> String {
    return "\(clazz)"
}

extension UIApplication {
    class func serviceLocator() -> ServiceLocator {
        return appDelegate().serviceLocator!
    }
    
    class func appDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    class func rootViewController() -> UINavigationController? {
        return appDelegate().window?.rootViewController as? UINavigationController
    }
}

class AppEnvironment {
    let environment : NSDictionary
    
    init(environmentDictionary: NSDictionary) {
        self.environment = environmentDictionary
    }
}

extension AppEnvironment {
    func useLocalServer() -> Bool {
        return (environment[AppEnvironmentUseLocalServerKey] as! NSNumber).boolValue
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var serviceLocator: ServiceLocator!
    var environment : AppEnvironment!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let environmentOptions = Bundle.main.object(forInfoDictionaryKey: EnvironmentInfoDictionarykey) as! NSDictionary
        UIApplication.shared.isIdleTimerDisabled = true
        
        environment = AppEnvironment(environmentDictionary: environmentOptions)
        
        // Override point for customization after application launch.
        serviceLocator = ServiceLocatorImpl()
        serviceLocator?.setServiceForName(protocolName(StopWatchController.self), serviceImplementation: StopWatchControllerImpl())
        
        serviceLocator.setServiceForName(className(TimeIntervalFormatter.self), serviceImplementation: TimeIntervalFormatter())
        
        let serverConfiguration = readServerConfiguration()
        let exerciseService = ExerciseServiceImpl(exerciseRepository: HttpExerciseRepository(configuration: serverConfiguration))
        serviceLocator?.setServiceForName(protocolName(ExerciseService.self), serviceImplementation: exerciseService)
        
        let audioService = AVAudioService()
        AVAudioServiceSoundsConfigurator.configureAudioService(audioService, withConfigurationFile: soundsConfigurationFilePath())
        audioService.set(tickSoundController: TickController())
        
        serviceLocator.setServiceForName(protocolName(AudioService.self), serviceImplementation: audioService)
        
        let routerService = ApplicationRouterServiceImpl(storyboard: UIStoryboard(name: "Main", bundle: nil), serviceLocator: serviceLocator)
        serviceLocator?.setServiceForName(protocolName(ApplicationRouterService.self), serviceImplementation: routerService)
        
        return true
    }
    
    func soundsConfigurationFilePath() -> String {
        return Bundle.main.path(forResource: "SoundsConfiguration", ofType: "plist")!
    }
    
    func readServerConfiguration() -> NSDictionary {
        let serverConfigurationFile = environment.useLocalServer() ? "LocalServerConfiguration" : "ServerConfiguration"
        
        return NSDictionary(contentsOfFile: Bundle.main.path(forResource: serverConfigurationFile, ofType: "plist")!)!
    }
}

