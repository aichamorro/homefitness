//
//  TimeIntervalFormatter.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 14/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

final class TimeIntervalFormatter {
    
    fileprivate let HHMMSSFormatter : DateComponentsFormatter
    
    init() {
        HHMMSSFormatter = DateComponentsFormatter()
        HHMMSSFormatter.allowedUnits = [.minute, .second]
        HHMMSSFormatter.zeroFormattingBehavior = DateComponentsFormatter.ZeroFormattingBehavior.pad
    }
    
    func toHHMMSS(_ timeInterval: TimeInterval) -> String {
        return HHMMSSFormatter.string(from: timeInterval)!
    }
}
