//
//  CountDownController.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 21/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

class CountDownController {
    fileprivate var _timerCompletion: Timer!
    fileprivate var _timerTicker: Timer!
    fileprivate var _didChangeSecondNotifier: () -> Void
    fileprivate var _completionBlock: () -> Void
    
    init(seconds: TimeInterval, didChangeSecondBlock: @escaping () -> Void, completionBlock: @escaping () -> Void) {
        _completionBlock = completionBlock
        _didChangeSecondNotifier = didChangeSecondBlock
    }
    
    class func countDown(seconds time: TimeInterval, startImmediately: Bool = false,
                                 didChangeSecondBlock secondsNotifier: @escaping () -> Void,
                                                      completionBlock: @escaping () -> Void) -> CountDownController
    {
        let counter = CountDownController(seconds: time,
                                          didChangeSecondBlock: secondsNotifier,
                                          completionBlock: completionBlock)

        counter._timerCompletion = Timer.scheduledTimer(timeInterval: time, target: counter, selector: #selector(timerDidComplete), userInfo: nil, repeats: false)

        counter._timerTicker = Timer.scheduledTimer(timeInterval: 1, target: counter, selector: #selector(timerTickDidUpdate), userInfo: nil, repeats: true)
        
        if startImmediately {
            counter._timerTicker.fire()
        }
        
        return counter
    }
    
    @objc func timerDidComplete(_ timer: Timer) {
        _timerTicker.invalidate()
        _timerCompletion.invalidate()
        
        _completionBlock()
    }
    
    @objc func timerTickDidUpdate(_ timer: Timer) {
        DispatchQueue.main.async { 
            self._didChangeSecondNotifier()
        }
    }
    
    func invalidate() {
        _timerTicker.invalidate()
        _timerCompletion.invalidate()
    }

}
