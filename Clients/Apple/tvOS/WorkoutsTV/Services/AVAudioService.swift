//
//  AVAudioService.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 12/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation
import AVFoundation

class AVAudioMusicPlayer : AudioMusicPlayer {
    let audioPlayer: AVAudioPlayer
    
    init(player: AVAudioPlayer) {
        audioPlayer = player
    }
    
    func play() {
        audioPlayer.play()
    }
    
    func resume() {
        audioPlayer.play()
    }
    
    func stop() {
        audioPlayer.stop()
    }
    
    func pause() {
        audioPlayer.pause()
    }
}

class AVAudioService : AudioService {
    
    var exerciseMusicPlayer: AudioMusicPlayer!
    var restMusicPlayer: AudioMusicPlayer!
    
    var tickController: TickController!
    var bellSound: AVAudioPlayer!
    var lowTickSound: AVAudioPlayer!
    var highTickSound: AVAudioPlayer!
    
    func set(tickSoundController: TickController) {
        self.tickController = tickSoundController
    }
    
    func set(bellSoundURL: URL) {
        bellSound = load(soundURL: bellSoundURL)
    }
    
    func set(lowTickSound: URL, highTickSound: URL) {
        self.lowTickSound = load(soundURL: lowTickSound)
        self.highTickSound = load(soundURL: highTickSound)
        
        self.lowTickSound.volume = 1.0
        self.highTickSound.volume = 1.0
    }
    
    func set(exerciseMusic: URL) {
        if let exerciseMusicSound = load(soundURL: exerciseMusic) {
            exerciseMusicSound.numberOfLoops = -1
            exerciseMusicSound.volume = 0.7
            
            exerciseMusicPlayer = AVAudioMusicPlayer(player: exerciseMusicSound)
        }
    }
    
    func set(restMusic: URL) {
        if let restMusicSound = load(soundURL: restMusic) {
            restMusicSound.numberOfLoops = -1
            restMusicSound.volume = 0.7
            
            restMusicPlayer = AVAudioMusicPlayer(player: restMusicSound)
        }
    }
    
    func load(soundURL url: URL) -> AVAudioPlayer? {
        return try? AVAudioPlayer(contentsOf: url)
    }
    
    func playTick() {
        if case .low = tickController.tick() {
            play(sound: lowTickSound)
        } else {
            play(sound: highTickSound)
        }
    }
    
    func playBell() {
        play(sound: bellSound)
    }
        
    fileprivate func play(sound: AVAudioPlayer) {
        sound.play()
    }
}

