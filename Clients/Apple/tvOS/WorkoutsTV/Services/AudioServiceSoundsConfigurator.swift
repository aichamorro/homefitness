//
//  AVAudioServiceSoundsConfigurator.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 12/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

final class AVAudioServiceSoundsConfigurator {
    
    fileprivate static let BellSoundFileKey = "BellSoundFile"
    fileprivate static let LowTickSoundFileKey = "LowTickSoundFile"
    fileprivate static let HighTickSoundFileKey = "HighTickSoundFile"
    fileprivate static let ExerciseMusicFileKey = "ExerciseMusicFile"
    fileprivate static let RestMusicFileKey = "RestMusicFile"
    
    class func configureAudioService(_ audioService: AVAudioService, withConfigurationFile configurationFile: String) {
        let configuration = NSDictionary(contentsOfFile: configurationFile)!
        
        if let bellSoundFile = configuration[BellSoundFileKey] as! NSString? {
            audioService.set(bellSoundURL: URLForSoundResource(bellSoundFile)!)
        }
        
        if let lowTickSoundFile = configuration[LowTickSoundFileKey] as! NSString?,
            let highTickSoundFile = configuration[HighTickSoundFileKey] as! NSString?
        {
            audioService.set(lowTickSound: URLForSoundResource(lowTickSoundFile)!,
                                         highTickSound: URLForSoundResource(highTickSoundFile)!)
        }
        
        if let exerciseMusicFile = configuration[ExerciseMusicFileKey] as! NSString? {
            audioService.set(exerciseMusic: URLForSoundResource(exerciseMusicFile)!)
        }
        
        if let restMusicFile = configuration[RestMusicFileKey] as! NSString? {
            audioService.set(restMusic: URLForSoundResource(restMusicFile)!)
        }
    }
    
    class func URLForSoundResource(_ filename: NSString) -> URL? {
        return Bundle.main.url(forResource: filename.deletingPathExtension, withExtension: filename.pathExtension)
    }
}
