//
//  ExerciseServiceImpl.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 02/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation
import RxSwift

class ExerciseServiceImpl : ExerciseService {
    let exerciseRepository: ExerciseRepository
    
    init(exerciseRepository: ExerciseRepository) {
        self.exerciseRepository = exerciseRepository
    }
    
    func getAllWorkouts() -> Observable<[Workout]?> {
        return exerciseRepository.findWorkoutAll()
    }
    
    func getWorkoutByName(_ name: String) -> Observable<Workout?> {
        return exerciseRepository.findWorkoutByName(name)
    }
    
    func getAllWorkoutNames() -> Observable<[String]?> {
        return exerciseRepository.findWorkoutAll().flatMap { workouts in
            return Observable.just(workouts?.map{$0.name})
        }
    }
}
