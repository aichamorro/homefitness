//
//  AudioService.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 12/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

protocol AudioMusicPlayer {
    func play()
    func stop()
    func pause()
    func resume()
}

protocol AudioService {
    var exerciseMusicPlayer: AudioMusicPlayer! { get }
    var restMusicPlayer: AudioMusicPlayer! { get }
    
    func playTick()
    func playBell()
}