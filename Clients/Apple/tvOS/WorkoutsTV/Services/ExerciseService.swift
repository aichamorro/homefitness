//
//  ExerciseService.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 02/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation
import RxSwift

protocol ExerciseService {
    func getWorkoutByName(_ name: String) -> Observable<Workout?>
    func getAllWorkouts() -> Observable<[Workout]?>
    func getAllWorkoutNames() -> Observable<[String]?>
}
