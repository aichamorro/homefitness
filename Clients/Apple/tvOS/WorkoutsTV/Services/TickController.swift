//
//  AVTickSoundController.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 12/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation
import AVFoundation

enum TickType {
    case high, low
}

class TickController {
    var nextTickType: TickType = .high
    
    func tick() -> TickType {
        let result = nextTickType
        
        updateNextTick()
        
        return result
    }
    
    fileprivate func updateNextTick() {
        switch nextTickType {
        case .high:
            nextTickType = .low
            
        case .low:
            nextTickType = .high
        }
    }
}
