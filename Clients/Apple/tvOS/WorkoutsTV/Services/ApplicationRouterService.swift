//
//  ApplicationRouterService.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 08/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

protocol ApplicationRouterService {
    func workoutDetails(_ workout: Workout, sender: AnyObject?)
}
