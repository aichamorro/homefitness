//
//  ApplicationRouterServiceImpl.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 08/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

final class ApplicationRouterServiceImpl : ApplicationRouterService {
    
    let storyboard: UIStoryboard
    weak var serviceLocator: ServiceLocator!
    
    init(storyboard: UIStoryboard, serviceLocator: ServiceLocator) {
        self.storyboard = storyboard
        self.serviceLocator = serviceLocator
    }
    
    func workoutDetails(_ workout: Workout, sender: AnyObject?) {
        let workoutDetailsController = storyboard.instantiateViewController(withIdentifier: className(ExerciseDetailViewController.self)) as! ExerciseDetailViewController
        workoutDetailsController.workout = workout
        workoutDetailsController.exerciseService = serviceLocator.getService(name: protocolName(ExerciseService.self)) as! ExerciseService
        workoutDetailsController.stopWatchController = serviceLocator.getService(name: protocolName(StopWatchController.self)) as! StopWatchController
        workoutDetailsController.audioService = serviceLocator.getService(name: protocolName(AudioService.self)) as! AudioService
        workoutDetailsController.timeFormatter = serviceLocator.getService(name: protocolName(TimeIntervalFormatter.self)) as! TimeIntervalFormatter

        let audioService = serviceLocator.getService(name: protocolName(AudioService.self)) as! AudioService
        
        let pauseController = storyboard.instantiateViewController(withIdentifier: className(PauseViewController.self)) as! PauseViewController
        
        let countDownController = storyboard.instantiateViewController(withIdentifier: className(CountDownViewController.self)) as! CountDownViewController
        countDownController.audioService = audioService
        
        let controllersCoordinator = ExerciseControllersCoordinator(audioService: audioService,
                                                                    pauseViewController: pauseController,
                                                                    countDownViewController: countDownController,
                                                                    exerciseDetailViewController: workoutDetailsController)
        
        workoutDetailsController.exerciseControllersCoordinator = controllersCoordinator
        pauseController.exerciseControllersCoordinator = controllersCoordinator
        countDownController.exerciseControllersCoordinator = controllersCoordinator
        
        controllersCoordinator.openExercise()
    }
}
