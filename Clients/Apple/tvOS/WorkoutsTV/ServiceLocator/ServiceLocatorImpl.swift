//
//  ServiceLocatorImpl.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 02/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

class ServiceLocatorImpl : ServiceLocator {
    var services: [String:AnyObject] = [:]
        
    func getService(name serviceName: String) -> AnyObject? {
        return services[serviceName]
    }
    
    func setServiceForName(_ serviceName: String, serviceImplementation: AnyObject) {
        services[serviceName] = serviceImplementation
    }
}
