//
//  ServiceLocator.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 02/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

protocol ServiceLocator: class {
    func getService(name serviceName: String) -> AnyObject?
    func setServiceForName(_ serviceName: String, serviceImplementation: AnyObject)
}
