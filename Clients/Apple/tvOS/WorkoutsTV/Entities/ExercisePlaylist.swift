//
//  ExercisePlaylist.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro on 14/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

class ExercisePlaylistFactory {
    class func playlistFromWorkout(_ workout: Workout) -> ExercisePlayerPlaylist {
        let playlistBuilder = ExercisePlayerPlaylist.builder()
        
        for currentSet in workout.workoutSets {
            playlistBuilder.addWorkoutSet(currentSet)
        }
        
        return playlistBuilder.build()
    }
}

class ExercisePlayerPlaylist {
    
    let exercises : [ExercisePlayerItem]
    var currentExerciseIndex = 0
    
    var nextExerciseIndex : Int {
        get {
            return currentExerciseIndex + 1
        }
    }
    
    var currentExercise : ExercisePlayerItem {
        get {
            return exercises[currentExerciseIndex]
        }
    }
    
    var nextExercise : ExercisePlayerItem? {
        get {
            return hasNextExercise ? exercises[nextExerciseIndex] : nil
        }
    }
    
    var hasNextExercise : Bool {
        get {
            return nextExerciseIndex < exercises.count
        }
    }
    
    var finished : Bool {
        get {
            return currentExerciseIndex >= exercises.count
        }
    }
    
    init(exercises: [ExercisePlayerItem]) {
        self.exercises = exercises
    }
    
    class func builder() -> Builder {
        return Builder()
    }
    
    func goToNextExercise() {
        currentExerciseIndex = currentExerciseIndex + 1
    }
    
    func restart() {
        currentExerciseIndex = 0
    }
    
    // MARK: Builder
    
    class Builder {
        var playlistExercises = [ExercisePlayerItem]()
        var currentWorkoutSet : [ExercisePlayerItem]?
        
        func startWorkoutSet() {
            currentWorkoutSet = [ExercisePlayerItem]()
        }
        
        func finishWorkoutSet() {
            playlistExercises.append(contentsOf: currentWorkoutSet!)
        }
        
        func addWorkoutSet(_ workoutSet: WorkoutSet) {
            startWorkoutSet()
            addExercises(workoutSet.exercises, exerciseDuration: workoutSet.exerciseDuration, exerciseRest: workoutSet.exerciseRest)
            repeatWorkoutSet(workoutSet.repetitions, restBetweenRepetitions: workoutSet.rest)
            finishWorkoutSet()
        }
        
        func build() -> ExercisePlayerPlaylist {
            return ExercisePlayerPlaylist(exercises: playlistExercises)
        }
        
        // MARK: WorkoutsBuilder methods
        
        func addExercises(_ exercises: [Exercise], exerciseDuration: TimeInterval, exerciseRest: TimeInterval) {
            for exercise in exercises {
                currentWorkoutSet?.append(ExercisePlayerItem(name: exercise.name, duration: exerciseDuration))
                
                if exerciseRest > 0 {
                    currentWorkoutSet?.append(ExercisePlayerItem(name: "Rest", duration: exerciseRest))
                }
            }
        }
        
        func repeatWorkoutSet(_ repetitions: Int, restBetweenRepetitions: TimeInterval) {
            guard repetitions > 0 else {
                return
            }
            
            var repeatedWorkoutSet = [ExercisePlayerItem]()
            for _ in 1...repetitions {
                repeatedWorkoutSet.append(contentsOf: currentWorkoutSet!)
                
                if restBetweenRepetitions > 0 {
                    repeatedWorkoutSet.append(ExercisePlayerItem(name: "Rest", duration: restBetweenRepetitions))
                }
            }
            
            if repeatedWorkoutSet.last?.name == "Rest" {
                repeatedWorkoutSet.removeLast()
            }
            
            currentWorkoutSet = repeatedWorkoutSet
        }
    }
}
