//
//  ExerciseList.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 02/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

protocol ExercisePlayerDelegate {
    func exercisePlayerDidFinish(_ player: ExercisePlayer)
    func exercisePlayerDidStart(_ player: ExercisePlayer)
    
    func exercisePlayer(_ player: ExercisePlayer, exerciseDidChange exercise: ExercisePlayerItem)
    func exercisePlayer(_ player: ExercisePlayer, didStartExercise exercise: ExercisePlayerItem)
    func exercisePlayer(_ player: ExercisePlayer, updateTime elapsed: TimeInterval, totalTime: TimeInterval)
    func exercisePlayer(_ player: ExercisePlayer, notifyProgress progress: Float)
}

 enum ExercisePlayerState {
    case notStarted
    case playing
    case paused
    case stopped
}

struct ExercisePlayerItem {
    let name: String
    let duration: TimeInterval
}

class ExercisePlayer {

    var state: ExercisePlayerState = .notStarted
    var delegate: ExercisePlayerDelegate?
    var stopwatch: StopWatchController?
    var playlist: ExercisePlayerPlaylist
        
    var hasNextExercise : Bool {
        get {
            return playlist.hasNextExercise
        }
    }
    
    var nextExercise : ExercisePlayerItem? {
        get {
            return playlist.nextExercise
        }
    }
    
    var currentExercise : ExercisePlayerItem {
        get {
            return playlist.currentExercise
        }
    }
    
    var hasStarted : Bool {
        get {
            return state != .notStarted
        }
    }
    
    var isRunning: Bool {
        get {
            return state == .playing
        }
    }
    
    init(playlist: ExercisePlayerPlaylist) {
        self.playlist = playlist
    }
    
    func start() {
        defer {
            self.delegate?.exercisePlayerDidStart(self)
        }
        
        guard state == .stopped || state == .notStarted else {
            resume()
            
            return
        }
        
        stopwatch?.delegate = self
        stopwatch?.reset(playlist.currentExercise.duration)
        stopwatch?.start()
        state = .playing
    }
    
    func pause() {
        state = .paused
        
        stopwatch?.pause()
    }
    
    func resume() {
        stopwatch!.resume()

        state = .playing
    }
    
    func stop() {
        state = .stopped
        stopwatch?.cancel()
        finishWorkout()
        reset()
    }
    
    func reset() {
        state = .notStarted
        playlist.restart()
        stopwatch?.reset(playlist.currentExercise.duration)
        updateCurrentExercise()
    }
    
    func goToNextExercise() {
        playlist.goToNextExercise()
        guard !playlist.finished else {
            delegate?.exercisePlayerDidFinish(self)
            
            return
        }

        updateCurrentExercise()
        startCurrentExercise()
    }
    
    func updateCurrentExercise() {
        delegate?.exercisePlayer(self, exerciseDidChange: playlist.currentExercise)
        delegate?.exercisePlayer(self, notifyProgress: 0)
    }
    
    func startCurrentExercise() {
        guard !playlist.finished else {
            return
        }
        
        stopwatch?.reset(playlist.currentExercise.duration)
        stopwatch?.start()
    }
    
    func finishWorkout() {
        delegate?.exercisePlayerDidFinish(self)
    }
}

extension ExercisePlayer: StopWatchControllerDelegate {
    func stopWatchControllerDidStart(_ controller: StopWatchController) {
        delegate?.exercisePlayer(self, didStartExercise: playlist.currentExercise)
    }
    
    func stopWatchControllerDidFinish(_ controller: StopWatchController, wasCanceled: Bool) {
        guard !wasCanceled else {
            return
        }
        
        goToNextExercise()
    }
    
    func stopWatchController(_ controller: StopWatchController, didReset time: TimeInterval) {
        delegate?.exercisePlayer(self, notifyProgress: 0)
    }
    
    func stopWatchController(_ controller: StopWatchController, didUpdate time: TimeInterval) {
        delegate?.exercisePlayer(self, updateTime: time, totalTime: playlist.currentExercise.duration)
        
        let progress = ((playlist.currentExercise.duration) - time)/(playlist.currentExercise.duration)
        delegate?.exercisePlayer(self, notifyProgress: Float(progress))
    }
}
