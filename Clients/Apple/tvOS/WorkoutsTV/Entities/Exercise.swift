//
//  Exercise.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 02/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

struct Exercise {
    let name: String
}