//
//  WorkoutSet.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro on 13/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

struct WorkoutSet {
    let rest: TimeInterval
    let repetitions: Int
    let exercises: [Exercise]
    let exerciseDuration: TimeInterval
    let exerciseRest: TimeInterval
}
