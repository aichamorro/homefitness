//
//  Workout.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 07/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

struct Workout {
    let id : UInt
    let name : String
    let imageUrl : String
    let duration : TimeInterval
    let workoutSets : [WorkoutSet]
}
