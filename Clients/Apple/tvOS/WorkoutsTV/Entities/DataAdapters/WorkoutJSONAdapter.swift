//
//  WorkoutJSONAdapter.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 08/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

func jsonArray(_ jsonObject: NSDictionary, key: String) -> NSArray {
    return jsonObject[key] as! NSArray
}

func jsonObject(_ jsonObject: NSDictionary, key: String) -> NSDictionary {
    return jsonObject[key] as! NSDictionary
}

class WorkoutJSONAdapter {
    static let WorkoutNameJSONKey = "name"
    static let WorkoutImageURLJSONKey = "imageURL"
    static let WorkoutExercisesJSONKey = "exercises"
    static let WorkoutDurationJSONKey = "duration"
    
    class func workoutFromJSON(_ json: NSDictionary) -> Workout {
        return Workout(id: 0,
                       name: json[WorkoutNameJSONKey] as! String,
                       imageUrl: json[WorkoutImageURLJSONKey] as! String,
                       duration: (json[WorkoutDurationJSONKey] as! NSNumber).doubleValue,
                       workoutSets: WorkoutSetJSONAdapter.workoutSetArrayFromJSON(jsonArray(json, key: "workoutSets")))
    }
    
    class func workoutArrayFromJSON(_ json: NSArray) -> [Workout] {
        var result : Array<Workout> = []
        
        for jsonWorkout in json {
            result.append(workoutFromJSON(jsonWorkout as! NSDictionary))
        }
        
        return result
    }
}
