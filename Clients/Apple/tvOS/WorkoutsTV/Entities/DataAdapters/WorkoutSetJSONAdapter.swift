//
//  WorkoutSetJSONAdapter.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro on 13/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

final class WorkoutSetJSONAdapter {
    class func workoutSetFromJSON(_ json: NSDictionary) -> WorkoutSet {
        let exercises = ExerciseJSONAdapter.exerciseArrayFromJSON(json["exercises"] as! NSArray)
        
        let workoutSet = WorkoutSet(rest: (json["rest"] as! NSNumber).doubleValue,
                                    repetitions: (json["repetitions"] as! NSNumber).intValue,
                                    exercises: exercises,
                                    exerciseDuration: (json["exerciseDuration"] as! NSNumber).doubleValue,
                                    exerciseRest: (json["exerciseRest"] as! NSNumber).doubleValue)
        
        return workoutSet
    }
    
    class func workoutSetArrayFromJSON(_ json: NSArray) -> [WorkoutSet] {
        var result = [WorkoutSet]()
        
        for jsonWorkoutSet in json {
            result.append(workoutSetFromJSON(jsonWorkoutSet as! NSDictionary))
        }
        
        return result
    }
}
