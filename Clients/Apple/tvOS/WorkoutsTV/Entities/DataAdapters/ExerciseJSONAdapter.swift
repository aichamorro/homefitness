//
//  ExerciseJSONAdapter.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 08/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

class ExerciseJSONAdapter {
    static let ExerciseNameJSONKey = "name"
    
    class func exerciseFromJSON(_ json: String) -> Exercise {
        return Exercise(name: json)
    }
    
    class func exerciseArrayFromJSON(_ json: NSArray) -> [Exercise] {
        var exercises : Array<Exercise> = []
        
        for jsonExercise in json {
            exercises.append(exerciseFromJSON(jsonExercise as! String))
        }
        
        return exercises
    }
}
