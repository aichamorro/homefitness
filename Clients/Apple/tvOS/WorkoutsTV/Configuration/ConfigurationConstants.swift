//
//  ConfigurationConstants.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 16/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

let HttpServerConfigurationURLKey = "ServerURL"
let HttpServerConfigurationWorkoutEndpointKey = "WorkoutsEndpoint"
let HttpServerConfigurationProtocolKey = "ServerProtocol"
let AppEnvironmentUseLocalServerKey = "UseLocalServer"
let EnvironmentInfoDictionarykey = "Environment"
