//
//  ServerEndpointsConfiguration.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 08/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

struct ServerEndpointsConfiguration {
    let workouts : String
    
    init(configuration: NSDictionary) {
        let httpProtocol = configuration[HttpServerConfigurationProtocolKey] as! String
        let serverUrl = configuration[HttpServerConfigurationURLKey] as! String

        workouts = "\(httpProtocol)\(serverUrl)/\(configuration[HttpServerConfigurationWorkoutEndpointKey]!)"
    }
}