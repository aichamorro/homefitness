//
//  ExerciseRepository.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 02/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation
import RxSwift

protocol ExerciseRepository {
    func findWorkoutByName(_ workoutName: String) -> Observable<Workout?>
    func findWorkoutAll() -> Observable<[Workout]?>
}
