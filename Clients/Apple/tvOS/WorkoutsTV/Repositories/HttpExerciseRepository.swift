//
//  HttpExerciseRepository.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 08/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

final class HttpExerciseRepository : ExerciseRepository {
    var workouts : [Workout]?
    
    let ServerEndpoints : ServerEndpointsConfiguration
    
    init(configuration: NSDictionary) {
        self.ServerEndpoints = ServerEndpointsConfiguration(configuration: configuration)
    }
    
    func findWorkoutAll() -> Observable<[Workout]?> {
        return Observable.create { [weak self] observer in
            guard let `self` = self else {
                observer.onCompleted()

                return Disposables.create {}
            }
            
            let request = Alamofire.request(self.ServerEndpoints.workouts).responseJSON { response in
                if response.result.isFailure {
                    observer.onError(response.result.error!)
                    
                    return
                }
                
                self.adaptAndSaveJSONWorkouts(response.result.value as! NSArray)
                
                observer.onNext(self.workouts)
                observer.onCompleted()
            }
            
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    fileprivate func adaptAndSaveJSONWorkouts(_ json: NSArray) {
        self.workouts = WorkoutJSONAdapter.workoutArrayFromJSON(json)
    }
    
    func findWorkoutByName(_ workoutName: String) -> Observable<Workout?> {
        return Observable.create { observer in
            let workout = self.workouts!.filter { $0.name == workoutName }.first
            
            observer.onNext(workout)
            observer.onCompleted()
            
            return Disposables.create { }
        }
    }
}
