//
//  StopWatchController.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 02/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

protocol StopWatchControllerDelegate {
    func stopWatchController(_ controller: StopWatchController, didUpdate time:(TimeInterval))
    func stopWatchController(_ controller: StopWatchController, didReset time:(TimeInterval))
    func stopWatchControllerDidStart(_ controller: StopWatchController)
    func stopWatchControllerDidFinish(_ controller: StopWatchController, wasCanceled: Bool)
}

protocol StopWatchController {
    var isRunning: Bool { get }
    var reverseTime: Bool { get set }
    var delegate: StopWatchControllerDelegate? {get set}
    
    func start()
    func pause()
    func resume()
    func cancel()
    func reset(_ time: TimeInterval)
}
