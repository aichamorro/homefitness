//
//  StopWatchControllerImpl.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 23/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

class StopWatchControllerImpl : StopWatchController {
    var isRunning = false
    var reverseTime = false
    var delegate: StopWatchControllerDelegate?
    var countDownController: CountDownController!
    var remaining: TimeInterval = 0.0
    
    func reset(_ time: TimeInterval) {
        if isRunning {
            cancel()
        }
        
        remaining = time
        delegate?.stopWatchController(self, didReset: remaining)
    }

    func start() {
        guard !isRunning else {
            NSLog("Warning: Tried to start a StopWatchController that's already started")
            
            return
        }
        
        startCountDown() {
            self.delegate?.stopWatchControllerDidStart(self)
        }
    }
    
    func pause() {
        guard isRunning else {
            NSLog("Warning: Tried to stop a StopWatchController that hasn't started")
            
            return
        }

        stopCountDown()
    }
    
    func resume() {
        guard !isRunning else {
            NSLog("Warning: Tried to resume a StopWatchController that's already started")
            
            return
        }

        startCountDown()
    }
    
    func cancel() {
        guard isRunning else {
            NSLog("Warning: Tried to cancel a StopWatchController that hasn't started")
            
            return
        }

        stopCountDown() {
            self.delegate?.stopWatchControllerDidFinish(self, wasCanceled: true)
        }
    }
    
    // MARK: Manage the CountDownController
    
    fileprivate func startCountDown(_ completionBlock: (() -> Void)? = nil) {
        countDownController = CountDownController.countDown(seconds: remaining, didChangeSecondBlock: {
            self.remaining -= 1
            self.delegate?.stopWatchController(self, didUpdate: self.remaining)
            }, completionBlock: {
                self.delegate?.stopWatchControllerDidFinish(self, wasCanceled: false)
        })
        
        isRunning = true
        
        completionBlock?()
    }
    
    fileprivate func stopCountDown(_ completionBlock: (() -> Void)? = nil) {
        countDownController.invalidate()
        isRunning = false
        
        completionBlock?()
    }
    
}
