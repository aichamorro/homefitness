//
//  CircularProgressBarView.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro on 06/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

@IBDesignable
class CircularProgressBarView : UIView {
    
    @IBInspectable var progress: Float = 0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var progressTintColor: UIColor = UIColor.black
    @IBInspectable var progressBackgroundColor: UIColor = UIColor.white
    @IBInspectable var strokeWidth: UInt = 1
    fileprivate let angleCorrection: CGFloat = CGFloat(M_PI_2)
        
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            fatalError("Couldn't retrieve the current graphics context..")
        }
        
        let centerPoint = CGPoint(x: rect.midX, y: rect.midY)
        let radius = min(rect.midX, rect.midY)

        drawBackground(center: centerPoint, radius: radius, context: context)
        drawProgressBar(center: centerPoint, radius: radius, context: context, progress: self.progress)
        clearFill(context, center: centerPoint, radius: radius)
    }
    
    internal func clearFill(_ context: CGContext, center centerPoint: CGPoint, radius: CGFloat) {
        let innerCircleRadius = radius - CGFloat(strokeWidth)
        
        context.setBlendMode(CGBlendMode.clear);
        
        drawProgressCircle(center: centerPoint, radius: innerCircleRadius, context: context, progress: 1)
    }
    
    internal func drawBackground(center centerPoint: CGPoint, radius: CGFloat, context: CGContext) {
        context.setFillColor(self.progressBackgroundColor.cgColor)
        
        drawProgressCircle(center: centerPoint, radius: radius, context: context, progress: 1)
    }
    
    internal func drawProgressBar(center centerPoint: CGPoint, radius: CGFloat, context: CGContext, progress: Float) {
        context.setFillColor(self.progressTintColor.cgColor)
        
        drawProgressCircle(center: centerPoint, radius: radius, context: context, progress: progress)
    }
    
    internal func drawProgressCircle(center centerPoint: CGPoint, radius: CGFloat, context: CGContext, progress: Float) {
        let path = CGMutablePath()
        path.move(to: centerPoint)
        path.addArc(center: centerPoint, radius: radius, startAngle: -self.angleCorrection, endAngle: progressToRadians(progress), clockwise: false)
        path.closeSubpath()
        
        context.addPath(path)
        context.fillPath()
    }
    
    internal func progressToRadians(_ progress: Float) -> CGFloat {
        return (CGFloat(progress) * CGFloat(2*M_PI)) - self.angleCorrection
    }
}
