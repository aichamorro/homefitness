//
//  ExerciseCollectionViewCell.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 07/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

final class ExerciseCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    fileprivate static let LabelRightMargin : CGFloat = 10
    fileprivate static let FocusScaleFactor: CGFloat = 1.1
    fileprivate static let UnfocusScaleFactor: CGFloat = 1.0
    
    func setDuration(_ durationString: String) {
        labelDuration.text = durationString
        labelDuration.sizeToFit()
        realignDurationLabel()
    }
    
    func realignDurationLabel() {
        let factor = self.isFocused ? type(of: self).FocusScaleFactor : type(of: self).UnfocusScaleFactor
        labelDuration.frame = CGRect(x: self.image.frame.width * factor - self.labelDuration.frame.width - type(of: self).LabelRightMargin,
                                     y: self.image.frame.height * factor - self.labelDuration.frame.height,
                                     width: self.labelDuration.frame.width,
                                     height: self.labelDuration.frame.height)
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        coordinator.addCoordinatedAnimations({
            self.labelName.isHidden = !self.isFocused
            self.labelName.alpha = self.isFocused ? 1 : 0
            
            self.labelDuration.transform = self.getViewScaleAffineTransform()
            self.realignDurationLabel()
        }) {
            self.labelName.isHidden = !self.isFocused
            self.labelName.alpha = self.isFocused ? 1 : 0
        }
    }
    
    fileprivate func getViewScaleAffineTransform() -> CGAffineTransform {
        return self.isFocused ?
            CGAffineTransform(scaleX: type(of: self).FocusScaleFactor, y: type(of: self).FocusScaleFactor) :
            CGAffineTransform.identity
    }
}
