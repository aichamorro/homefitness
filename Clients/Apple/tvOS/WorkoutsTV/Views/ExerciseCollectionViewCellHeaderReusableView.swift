//
//  ExerciseCollectionViewCellHeaderResuableView.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 07/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

class ExerciseCollectionViewCellHeaderReusableView : UICollectionReusableView {
    
    @IBOutlet var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        titleLabel.font = UIFont.systemFont(ofSize: 39)
        
        self.addSubview(titleLabel)
    }
    
    func setTitle(_ title: String) {
        titleLabel.text = title
        titleLabel.sizeToFit()
        
        self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: titleLabel.frame.width, height: titleLabel.frame.height)
        
        setNeedsLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
