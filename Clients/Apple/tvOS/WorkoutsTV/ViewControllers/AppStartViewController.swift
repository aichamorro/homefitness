//
//  AppStartViewController.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 07/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit
import RxSwift

let ExerciseCollectionViewCellReusableIdentifier = "AppStartViewControllerCollectionViewCell"
let ExerciseCollectionViewCellHeaderReusableIdentifier = "AppStartViewControllerCollectionViewCellHeader"

class AppStartViewController : UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionViewWorkouts: UICollectionView!
    var workouts: [String]!
    var exerciseService: ExerciseService!
    var timeFormatter: TimeIntervalFormatter!
    private final var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        configureUI()
        configureServices()
        loadData()
    }
    
    func configureUI() {
        self.collectionViewWorkouts.register(UINib(nibName: "ExerciseCollectionViewCell", bundle: nil),forCellWithReuseIdentifier: ExerciseCollectionViewCellReusableIdentifier)
        
        self.collectionViewWorkouts.register(ExerciseCollectionViewCellHeaderReusableView.self,
                                                  forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                                  withReuseIdentifier: ExerciseCollectionViewCellHeaderReusableIdentifier)
    }
    
    func configureServices() {
        let serviceLocator = UIApplication.serviceLocator();
        
        timeFormatter = serviceLocator.getService(name: protocolName(TimeIntervalFormatter.self)) as! TimeIntervalFormatter
        exerciseService = serviceLocator.getService(name: protocolName(ExerciseService.self)) as! ExerciseService
    }
    
    func loadData() {
        workouts = []

        exerciseService.getAllWorkoutNames().subscribe(onNext: { workoutNames in
            self.workouts = workoutNames
            
            self.collectionViewWorkouts.reloadData()
        }).addDisposableTo(disposeBag)
    }
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        exerciseService.getWorkoutByName(workouts[indexPath.row]).subscribe(onNext: { workout in
            let routerService = UIApplication.serviceLocator().getService(name: protocolName(ApplicationRouterService.self)) as! ApplicationRouterService
            
            routerService.workoutDetails(workout!, sender: self)
        }).addDisposableTo(disposeBag)
    }
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.workouts.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExerciseCollectionViewCellReusableIdentifier, for: indexPath) as! ExerciseCollectionViewCell
        
        exerciseService.getWorkoutByName(workouts[indexPath.row]).subscribe(onNext: { workout in
            cell.labelName.text = workout?.name
            cell.labelDuration.text = self.timeFormatter.toHHMMSS((workout?.duration)!)
            
            DispatchQueue.global(qos: .userInteractive).async {
                let image = URL(string: (workout?.imageUrl)!)
                    .flatMap { (try? Data(contentsOf: $0)) }
                    .flatMap { UIImage(data: $0) }
                
                DispatchQueue.main.async {
                    cell.image.image = image
                }
            }
        }).addDisposableTo(disposeBag)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: ExerciseCollectionViewCellHeaderReusableIdentifier, for: indexPath) as! ExerciseCollectionViewCellHeaderReusableView
        
            header.setTitle("Workouts")
        
            return header
        }

        fatalError("This should not happen since we haven't configured the Footer")
    }
        
    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 300, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 60
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 10, height: 10)
    }
    
}
