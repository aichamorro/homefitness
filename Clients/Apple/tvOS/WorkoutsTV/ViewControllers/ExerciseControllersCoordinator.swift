//
//  ExerciseControllersCoordinator.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 24/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation
import UIKit

class ExerciseControllersCoordinator {
    let audioService: AudioService!
    var pauseViewController: PauseViewController!
    var countDownViewController: CountDownViewController!
    weak var exerciseDetailViewController: ExerciseDetailViewController!
    
    init(audioService: AudioService,
         pauseViewController: PauseViewController,
         countDownViewController: CountDownViewController,
         exerciseDetailViewController: ExerciseDetailViewController)
    {
        self.audioService = audioService
        self.pauseViewController = pauseViewController
        self.pauseViewController.modalPresentationStyle = .overFullScreen
        self.pauseViewController.modalTransitionStyle = .crossDissolve
        
        self.countDownViewController = countDownViewController
        self.countDownViewController.modalPresentationStyle = .overFullScreen
        self.countDownViewController.modalTransitionStyle = .coverVertical

        self.exerciseDetailViewController = exerciseDetailViewController
    }
    
    func openExercise() {
        UIApplication.rootViewController()?.pushViewController(exerciseDetailViewController, animated: true)
    }
    
    func startWorkout() {
        exerciseDetailViewController.present(countDownViewController, animated: true, completion: nil)
    }
    
    func didFinishCountDown() {
        countDownViewController.dismiss(animated: true, completion: nil)
        exerciseDetailViewController.startWorkout()
    }
    
    func pauseExercise() {
        showPauseOverlay()

        audioService.exerciseMusicPlayer.pause()
        audioService.restMusicPlayer.play()
    }
    
    func resumeWorkout() {
        hidePauseOverlay()

        audioService.exerciseMusicPlayer.resume()
        audioService.restMusicPlayer.stop()
        
        exerciseDetailViewController.resumeWorkout()
    }
    
    fileprivate func showPauseOverlay() {
        self.exerciseDetailViewController.present(pauseViewController, animated: true, completion: nil)
    }
    
    fileprivate func hidePauseOverlay() {
        self.pauseViewController.dismiss(animated: true, completion: nil)
    }
    
    func cancelWorkout() {
    }
    
    fileprivate func didChangeExercise() {
        audioService.playBell()
    }
    
    func didStartExerciseInterval() {
        didChangeExercise()
        
        audioService.restMusicPlayer.stop()
        audioService.exerciseMusicPlayer.play()
    }
    
    func didStartRestInterval() {
        didChangeExercise()
        
        audioService.restMusicPlayer.play()
        audioService.exerciseMusicPlayer.stop()
    }
    
    func didFinishWorkout() {
        audioService.restMusicPlayer.stop()
        audioService.exerciseMusicPlayer.stop()
        notifyWorkoutCompleted()
    }
    
    func notifyWorkoutCompleted() {
        let alertController = UIAlertController(title: NSLocalizedString("Workout finished", comment: "Workout finished"),
                                                message: NSLocalizedString("The workout has finished", comment: "The workout has finished"), preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok"),
            style: UIAlertActionStyle.default, handler: { (_) in
                alertController.dismiss(animated: true, completion: nil)
        }))
        
        self.exerciseDetailViewController.present(alertController, animated: true, completion: nil)
    }
}
