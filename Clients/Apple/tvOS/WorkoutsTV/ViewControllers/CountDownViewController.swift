//
//  CountDownViewController.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 21/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation
import UIKit

class CountDownViewController : UIViewController {
    @IBOutlet weak var labelCount: UILabel!
    fileprivate var countDownController: CountDownController?
    fileprivate var currentTime = 3
    var audioService: AudioService!
    var completionBlock: (() -> Void)!
    var exerciseControllersCoordinator: ExerciseControllersCoordinator!
    
    override func viewWillAppear(_ animated: Bool) {
        updateCountLabel()
    }
    
    func updateCountLabel() {
        labelCount.text = "\(self.currentTime)"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        countDownController = CountDownController.countDown(seconds: 3, startImmediately: true, didChangeSecondBlock: {
            self.updateCountLabel()
            self.currentTime = self.currentTime - 1
            self.audioService.playTick()
        }, completionBlock: {
            self.exerciseControllersCoordinator.didFinishCountDown()
        })
    }
}
