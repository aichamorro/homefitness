//
//  PauseViewController.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 23/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

class PauseViewController : UIViewController {
    
    weak var exerciseControllersCoordinator: ExerciseControllersCoordinator!
    
    override func pressesEnded(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        for item in presses {
            if item.type == .playPause {
                exerciseControllersCoordinator.resumeWorkout()
            }
        }
    }
}
