//
//  ExerciseDetailViewController.swift
//  WorkoutsTV
//
//  Created by Alberto Chamorro - Personal on 04/05/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit
import AVFoundation

class ExerciseDetailViewController : UIViewController {
    fileprivate let SecondsToFinishForTickingSound: TimeInterval = 5
    fileprivate let RestExerciseName = "rest"
    
    @IBOutlet weak var labelCurrentExercise: UILabel!
    @IBOutlet weak var labelNextExercise: UILabel!
    @IBOutlet weak var buttonStartStop: UIButton!
    @IBOutlet weak var textFieldTimeIndicator: UITextField!
    @IBOutlet weak var progressBar: CircularProgressBarView!
    var exerciseControllersCoordinator: ExerciseControllersCoordinator!
    var exercisePlayer: ExercisePlayer!
    var exerciseService: ExerciseService!
    var audioService: AudioService!
    var workout: Workout!
    var timeFormatter: TimeIntervalFormatter!
    var stopWatchController: StopWatchController!
    var isWorkoutLoaded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        stopWatchController.reverseTime = true
        
        let exercisesPlaylist = ExercisePlaylistFactory.playlistFromWorkout(workout)
        exercisePlayer = ExercisePlayer(playlist: exercisesPlaylist)
        exercisePlayer.stopwatch = stopWatchController
        exercisePlayer.delegate = self
        labelCurrentExercise.text = exercisePlayer.currentExercise.name
        textFieldTimeIndicator.text = timeFormatter.toHHMMSS(exercisePlayer.currentExercise.duration)
        labelNextExercise.text = exercisePlayer.hasNextExercise ? exercisePlayer.nextExercise?.name : NSLocalizedString("End", comment: "End")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionStartStop(_ sender: AnyObject) {
        if exercisePlayer.isRunning {
            exercisePlayer.stop()
            
            exerciseControllersCoordinator.cancelWorkout()
        } else {
            exerciseControllersCoordinator.startWorkout()
        }
    }
    
    func startWorkout() {
        exercisePlayer.start()
    }
    
    func resumeWorkout() {
        exercisePlayer.resume()
    }
}

extension ExerciseDetailViewController: ExercisePlayerDelegate {
    func exercisePlayerDidStart(_ player: ExercisePlayer) {
        buttonStartStop.setTitle(NSLocalizedString("Cancel", comment: "Cancel"), for: UIControlState())
    }
    
    func exercisePlayer(_ player: ExercisePlayer, updateTime elapsed: TimeInterval, totalTime: TimeInterval) {
        if elapsed <= SecondsToFinishForTickingSound {
            audioService.playTick()
        }
        
        textFieldTimeIndicator.text = timeFormatter.toHHMMSS(elapsed)
    }
    
    func exercisePlayer(_ player: ExercisePlayer, notifyProgress progress: Float) {
        progressBar.progress = progress
    }
    
    func exercisePlayer(_ player: ExercisePlayer, didStartExercise exercise: ExercisePlayerItem) {
        if exercise.name.lowercased() != RestExerciseName {
            exerciseControllersCoordinator.didStartExerciseInterval()
        } else {
            exerciseControllersCoordinator.didStartRestInterval()
        }
    }
    
    func exercisePlayer(_ player: ExercisePlayer, exerciseDidChange exercise: ExercisePlayerItem) {
        labelCurrentExercise.text = exercise.name
        labelNextExercise.text = player.hasNextExercise ? exercisePlayer.nextExercise?.name : NSLocalizedString("End", comment: "End")
        textFieldTimeIndicator.text = timeFormatter.toHHMMSS(exercise.duration)
    }
    
    func exercisePlayerDidFinish(_ player: ExercisePlayer) {
        exerciseControllersCoordinator.didFinishWorkout()

        player.reset()
        self.buttonStartStop.setTitle(NSLocalizedString("Start", comment: "Start"), for: UIControlState())
    }
    
}

extension ExerciseDetailViewController {
    override func pressesEnded(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        for item in presses {
            if item.type == .playPause {
                guard exercisePlayer.hasStarted else {
                    return
                }

                exercisePlayer.pause()
                exerciseControllersCoordinator.pauseExercise()
            }
        }
    }
}




