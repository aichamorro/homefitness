package com.fitnesse.workouts.entities;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;

public final class ExerciseJsonAdatper {

	public static JsonArray exerciseArray(List<Exercise> exercises) {
		JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
		
		for (Exercise exercise : exercises) {
			jsonArrayBuilder.add(exerciseToJson(exercise));
		}
		
		return jsonArrayBuilder.build();
	}

	private static String exerciseToJson(Exercise exercise) {
		return exercise.getName();
	}

	public static List<Exercise> exerciseArrayFromJson(JsonArray exercisesJson) {
		ArrayList<Exercise> result = new ArrayList<>(exercisesJson.size());
		
		for (int i=0;i<exercisesJson.size(); i++) {
			result.add(exerciseFromJson(exercisesJson.getString(i)));
		}
		
		return result;
	}

	private static Exercise exerciseFromJson(String exerciseJson) {
		return Exercise.builder()
				.name(exerciseJson)
				.build();
	}

}
