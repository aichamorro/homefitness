package com.fitnesse.workouts.entities;

import java.util.List;

public final class Workout<T> {
	T id;
	String name;
	Long duration;
	String imageUrl;
	List<WorkoutSet> workoutSets;

	public T getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public Long getDuration() {
		return duration;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public List<WorkoutSet> getWorkoutSets() {
		return workoutSets;
	}
	
	static public <E> Builder<E> builder() {
		return new Builder<E>();
	}
	
	static public class Builder<E> {
		Workout<E> workout;
		
		Builder() {
			workout = new Workout<E>();
		}
		
		public Builder<E> id(E id) {
			workout.id = id;
			
			return this;
		}
		
		public Builder<E> name(String name) {
			workout.name = name;
			
			return this;
		}
		
		public Builder<E> duration(Long duration) {
			workout.duration = duration;
			
			return this;
		}
		
		public Builder<E> imageUrl(String imageUrl) {
			workout.imageUrl = imageUrl;
			
			return this;
		}
		
		public Builder<E> workoutSets(List<WorkoutSet> workoutSets) {
			workout.workoutSets = workoutSets;
			
			return this;
		}
		
		public Workout<E> build() {
			return workout;
		}
	}
}
