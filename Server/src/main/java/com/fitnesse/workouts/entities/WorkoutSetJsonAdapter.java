package com.fitnesse.workouts.entities;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import com.fitnesse.workouts.api.WorkoutsApiSpecification;

public final class WorkoutSetJsonAdapter {

	public static JsonValue workoutSetArray(List<WorkoutSet> workoutSets) {
		JsonArrayBuilder jsonBuilder = Json.createArrayBuilder();
		
		for (int i=0; i<workoutSets.size(); i++) {
			jsonBuilder.add(jsonWorkoutSet(workoutSets.get(i)));
		}
		
		return jsonBuilder.build();
	}

	private static JsonValue jsonWorkoutSet(WorkoutSet workoutSet) {
		JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
		
		jsonObjectBuilder.add(WorkoutsApiSpecification.WorkoutSetJsonParamNames.Rest, workoutSet.getRest());
		jsonObjectBuilder.add(WorkoutsApiSpecification.WorkoutSetJsonParamNames.Repetitions, workoutSet.getRepetitions());
		jsonObjectBuilder.add(WorkoutsApiSpecification.WorkoutSetJsonParamNames.ExerciseDuration, workoutSet.getExerciseDuration());
		jsonObjectBuilder.add(WorkoutsApiSpecification.WorkoutSetJsonParamNames.ExerciseRest, workoutSet.getExerciseRest());
		jsonObjectBuilder.add(WorkoutsApiSpecification.WorkoutSetJsonParamNames.Exercises, ExerciseJsonAdatper.exerciseArray(workoutSet.getExercises()));
		
		return jsonObjectBuilder.build();
	}

	public static WorkoutSet workoutSetFromJson(JsonObject workoutSetJson) {
		List<Exercise> exercises = ExerciseJsonAdatper.exerciseArrayFromJson(workoutSetJson.getJsonArray(WorkoutsApiSpecification.WorkoutSetJsonParamNames.Exercises));
		
		return WorkoutSet.builder()
							.exercises(exercises)
							.repetitions(new Long(workoutSetJson.getInt(WorkoutsApiSpecification.WorkoutSetJsonParamNames.Repetitions)))
							.exerciseDuration(new Long(workoutSetJson.getInt(WorkoutsApiSpecification.WorkoutSetJsonParamNames.ExerciseDuration)))
							.exerciseRest(new Long(workoutSetJson.getInt(WorkoutsApiSpecification.WorkoutSetJsonParamNames.ExerciseRest)))
							.rest(new Long(workoutSetJson.getInt(WorkoutsApiSpecification.WorkoutSetJsonParamNames.Rest)))
							.build();
	}

	public static List<WorkoutSet> workoutSetArrayFromJson(JsonArray workoutSetsJson) {
		ArrayList<WorkoutSet> workoutSets = new ArrayList<>(workoutSetsJson.size());
		
		for (int i=0; i<workoutSetsJson.size(); i++) {
			workoutSets.add(workoutSetFromJson(workoutSetsJson.getJsonObject(i)));
		}
	
		return workoutSets;
	}

}
