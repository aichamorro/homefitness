package com.fitnesse.workouts.entities;

public final class Exercise {
	String name;
	
	public static final Builder builder() {
		return new Builder();
	}
	
	public String getName() {
		return name;
	}
		
	static public class Builder {
		Exercise exercise;
		
		public Builder() {
			exercise = new Exercise();
		}
		
		public Builder name(String name) {
			exercise.name = name;
			
			return this;
		}
				
		public Exercise build() {
			return exercise;
		}
	}
}
