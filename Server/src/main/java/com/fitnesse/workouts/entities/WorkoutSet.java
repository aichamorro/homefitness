package com.fitnesse.workouts.entities;

import java.util.List;

public final class WorkoutSet {
	
	private Long repetitions;
	private Long rest;
	private List<Exercise> exercises;
	private Long exerciseDuration;
	private Long exerciseRest;

	public Long getRepetitions() {
		return repetitions;
	}

	public Long getRest() {
		return rest;
	}

	public List<Exercise> getExercises() {
		return exercises;
	}
	
	public Long getExerciseDuration() {
		return exerciseDuration;
	}
	
	public Long getExerciseRest() {
		return exerciseRest;
	}

	public static Builder builder() {
		return new Builder();
	}
	
	public Long getTotalDuration() {
		 return ((getExerciseRest() + getExerciseDuration()) * exercises.size() + getRest()) * getRepetitions();
	}
	
	public static class Builder {
		
		private WorkoutSet workoutSet;

		public Builder() {
			workoutSet = new WorkoutSet();
		}

		public Builder repetitions(Long repetitions) {
			workoutSet.repetitions = repetitions;
			
			return this;
		}

		public Builder rest(Long restTime) {
			workoutSet.rest = restTime;
			
			return this;
		}

		public Builder exercises(List<Exercise> exercises) {
			workoutSet.exercises = exercises;
			
			return this;
		}
		
		public Builder exerciseDuration(Long exerciseDuration) {
			workoutSet.exerciseDuration = exerciseDuration;
			
			return this;
		}
		
		public Builder exerciseRest(Long restDuration) {
			workoutSet.exerciseRest = restDuration;
			
			return this;
		}
		
		public WorkoutSet build() {
			return workoutSet;
		}
	}
}
