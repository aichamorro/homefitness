package com.fitnesse.workouts.entities;

import java.util.Iterator;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import com.fitnesse.workouts.api.WorkoutsApiSpecification;

public final class WorkoutJsonAdapter {

	public static JsonArray jsonWorkoutArray(Iterator<Workout<?>> workouts) {
		JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
		
		while (workouts.hasNext()) {
			jsonArrayBuilder.add(jsonWorkout(workouts.next()));
		}
		
		return jsonArrayBuilder.build();
	}
	
	public static JsonObject jsonWorkout(Workout<?> workout) {
		JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
		
		jsonObjectBuilder.add(WorkoutsApiSpecification.WorkoutJsonParamNames.Name, workout.getName());
		jsonObjectBuilder.add(WorkoutsApiSpecification.WorkoutJsonParamNames.Duration, workout.getDuration());
		jsonObjectBuilder.add(WorkoutsApiSpecification.WorkoutJsonParamNames.Image, workout.getImageUrl());
		jsonObjectBuilder.add(WorkoutsApiSpecification.WorkoutJsonParamNames.Sets, WorkoutSetJsonAdapter.workoutSetArray(workout.workoutSets));

		return jsonObjectBuilder.build();
	}

	public static Workout<?> workoutFromJson(JsonObject workoutJson) {
		JsonArray workoutSetsJson = workoutJson.getJsonArray(WorkoutsApiSpecification.WorkoutJsonParamNames.Sets);
		List<WorkoutSet> workoutSets = WorkoutSetJsonAdapter.workoutSetArrayFromJson(workoutSetsJson);
		Workout<?> result = Workout.builder()
									.duration(new Long(workoutJson.getInt(WorkoutsApiSpecification.WorkoutJsonParamNames.Duration)))
									.imageUrl(workoutJson.getString(WorkoutsApiSpecification.WorkoutJsonParamNames.Image))
									.name(workoutJson.getString(WorkoutsApiSpecification.WorkoutJsonParamNames.Name))
									.workoutSets(workoutSets)
									.build();
		
		return result;
	}
	
}
