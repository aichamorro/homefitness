package com.fitnesse.workouts.api;

public final class WorkoutsApiSpecification {
	final public static class WorkoutJsonParamNames {
		public static final String Name = "name";
		public static final String Image = "imageURL";
		public static final String Duration = "duration";
		public static final String Sets = "workoutSets";
	}
		
	final public static class WorkoutSetJsonParamNames {
		public static final String Repetitions = "repetitions";
		public static final String Rest = "rest";
		public static final String Exercises = "exercises";
		public static final String ExerciseDuration = "exerciseDuration";
		public static final String ExerciseRest = "exerciseRest";

	}

}
