package com.fitnesse.workouts.repositories;

import java.util.Iterator;

import com.fitnesse.common.QueryResultsToIteratorAdapter;
import com.fitnesse.workouts.repositories.datastore.entities.ExerciseDatastoreEntity;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.FullEntity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.Query;
import com.google.cloud.datastore.StructuredQuery.OrderBy;
import com.google.cloud.datastore.StructuredQuery.PropertyFilter;

public final class DatastoreExercisesRepository implements ExercisesRepository<Key, FullEntity<?>> {

	private Datastore datastore;

	public DatastoreExercisesRepository(Datastore datastore) {
		this.datastore = datastore;
	}

	@Override
	public Iterator<FullEntity<?>> findByWorkoutSet(Key workoutSetKey) {
		Query<Entity> query = buildExercisesQuery_FindByWorkoutSet(workoutSetKey);
		
		return new QueryResultsToIteratorAdapter(datastore.run(query));
	}
	
	@Override
	public Entity save(FullEntity<?> exercisesDatastoreEntity) {
		return datastore.add(exercisesDatastoreEntity);
	}
		
	Query<Entity> buildExercisesQuery_FindByWorkoutSet(Key workutSetKey) {
		return Query.entityQueryBuilder()
				.kind(ExerciseDatastoreEntity.Kind)
				.filter(PropertyFilter.eq(ExerciseDatastoreEntity.EntityFields.WorkoutSet, workutSetKey))
				.orderBy(OrderBy.asc(ExerciseDatastoreEntity.EntityFields.Order))
				.build();
	}

	@Override
	public void saveAll(FullEntity<?>[] exercisesDatastoreEntity) {
		datastore.add(exercisesDatastoreEntity);
	}

}
