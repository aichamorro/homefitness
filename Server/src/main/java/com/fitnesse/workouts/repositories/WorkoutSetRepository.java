package com.fitnesse.workouts.repositories;

import java.util.Iterator;

public interface WorkoutSetRepository<I, T> {
	public Iterator<T> findByWorkout(I workoutId);
	public T save(T workoutSetDatastoreEntity);
}
