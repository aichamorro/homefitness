package com.fitnesse.workouts.repositories;

import java.util.Iterator;

public interface WorkoutsRepository<I, T> {
	public Iterator<T> findAll();
	public T save(T workoutDatastore);
}
