package com.fitnesse.workouts.repositories;

import java.util.Iterator;

public interface ExercisesRepository<I, C> {
	public Iterator<C> findByWorkoutSet(I id);
	public C save(C exercisesDatastoreEntity);
	public void saveAll(C[] exercisesDatastoreEntity);
}
