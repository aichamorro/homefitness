package com.fitnesse.workouts.repositories;

import java.util.Iterator;

import com.fitnesse.common.QueryResultsToIteratorAdapter;
import com.fitnesse.workouts.repositories.datastore.entities.WorkoutSetDatastoreEntity;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.FullEntity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.Query;
import com.google.cloud.datastore.StructuredQuery.OrderBy;
import com.google.cloud.datastore.StructuredQuery.PropertyFilter;

public final class DatastoreWorkoutSetRepository implements WorkoutSetRepository<Key, FullEntity<?>> {

	private Datastore datastore;

	public DatastoreWorkoutSetRepository(Datastore datastore) {
		this.datastore = datastore;
	}

	@Override
	public Iterator<FullEntity<?>> findByWorkout(Key workoutId) {
		Query<Entity> query = buildWorkoutSetQuery_FindByWorkout(workoutId);

		return new QueryResultsToIteratorAdapter(datastore.run(query));
	}
	
	Query<Entity> buildWorkoutSetQuery_FindByWorkout(Key workoutId) {
		return Query.entityQueryBuilder().kind(WorkoutSetDatastoreEntity.Kind)
											.filter(PropertyFilter.eq(WorkoutSetDatastoreEntity.EntityFields.Workout, workoutId))
											.orderBy(OrderBy.asc(WorkoutSetDatastoreEntity.EntityFields.Order))
											.build();
	}

	@Override
	public Entity save(FullEntity<?> workoutSetDatastoreEntity) {
		return datastore.add(workoutSetDatastoreEntity);
	}
	
}
