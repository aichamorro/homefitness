package com.fitnesse.workouts.repositories.datastore.entities;

import java.util.ArrayList;
import java.util.List;

import com.fitnesse.workouts.entities.Exercise;
import com.fitnesse.workouts.entities.Workout;
import com.fitnesse.workouts.entities.WorkoutSet;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.FullEntity;
import com.google.cloud.datastore.Key;

public final class DatastoreEntityAdapter {
	static public Workout<?> workoutEntityToWorkout(FullEntity<?> workoutEntity,
			List<WorkoutSet> workoutSets)
	{
		Long totalDuration = 0L;
		for (WorkoutSet workoutSet : workoutSets) {
			totalDuration += workoutSet.getTotalDuration();
		}
		
		return Workout.<Key> builder()
				.name(WorkoutDatastoreEntity.getName(workoutEntity))
				.duration(totalDuration)
				.imageUrl(WorkoutDatastoreEntity.getImageUrl(workoutEntity))
				.workoutSets(workoutSets)
				.build();
	}

	static public WorkoutSet workoutSetEntityToWorkoutSet(FullEntity<?> workoutSetEntity,
			List<Exercise> exercises)
	{
		return WorkoutSet.builder()
				.repetitions(WorkoutSetDatastoreEntity.getIterations(workoutSetEntity))
				.rest(WorkoutSetDatastoreEntity.getRest(workoutSetEntity))
				.exerciseDuration(WorkoutSetDatastoreEntity.getExerciseDuration(workoutSetEntity))
				.exerciseRest(WorkoutSetDatastoreEntity.getExerciseRest(workoutSetEntity))
				.exercises(exercises)
				.build();
	}

	static public Exercise exerciseEntityToExercise(FullEntity<?> exerciseEntity)
	{
		return Exercise.builder()
				.name(ExerciseDatastoreEntity.getName(exerciseEntity))
				.build();
	}

	static public List<Exercise> convertToExerciseList(List<Entity> exercises)
	{
		ArrayList<Exercise> result = new ArrayList<>();

		for (Entity exercise : exercises) {
			result.add(DatastoreEntityAdapter.exerciseEntityToExercise(exercise));
		}

		return result;
	}
}
