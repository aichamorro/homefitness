package com.fitnesse.workouts.repositories.datastore.entities;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.FullEntity;
import com.google.cloud.datastore.IncompleteKey;
import com.google.cloud.datastore.Key;

public final class WorkoutSetDatastoreEntity {
	static public final String Kind = "WorkoutSet";
	
	final public class EntityFields {
		static public final String Iterations = "iterations";
		static public final String Rest = "rest";
		static public final String Workout = "workout";
		public static final String Order = "order";
		public static final String ExerciseDuration = "exerciseDuration";
		public static final String ExerciseRest = "exerciseRest";
	}
	
	public static Key getKey(FullEntity<?> entity) {
		return entity instanceof Entity ? ((Entity)entity).key() : null;
	}
		
	static public Long getRest(FullEntity<?> entity) {
		return entity.getLong(EntityFields.Rest);
	}
	
	static public Key getWorkout(FullEntity<?> entity) {
		return entity.getKey(EntityFields.Workout);
	}

	static public Long getIterations(FullEntity<?> entity) {
		return entity.getLong(EntityFields.Iterations);
	}
	
	static public Long getOrder(FullEntity<?> entity) {
		return entity.getLong(EntityFields.Order);
	}
	
	static public Long getExerciseDuration(FullEntity<?> entity) {
		return entity.getLong(EntityFields.ExerciseDuration);
	}
	
	static public Long getExerciseRest(FullEntity<?> entity) {
		return entity.getLong(EntityFields.ExerciseRest);
	}

	
	public static Builder builder(Datastore datastore) {
		return new Builder(datastore.newKeyFactory().kind(Kind).newKey());
	}
	
	public static class Builder {
		private com.google.cloud.datastore.FullEntity.Builder<IncompleteKey> entityBuilder;

		public Builder(IncompleteKey newKey) {
			entityBuilder = Entity.builder(newKey);
		}
		
		public Builder iterations(Long iterations) {
			entityBuilder.set(EntityFields.Iterations, iterations);
			
			return this;
		}
		
		public Builder rest(Long rest) {
			entityBuilder.set(EntityFields.Rest, rest);
			
			return this;
		}
		
		public Builder workout(Key workoutId) {
			entityBuilder.set(EntityFields.Workout, workoutId);
			
			return this;
		}
		
		public Builder exerciseRest(Long exerciseRest) {
			entityBuilder.set(EntityFields.ExerciseRest, exerciseRest);
			
			return this;
		}
		
		public Builder exerciseDuration(Long exerciseDuration) {
			entityBuilder.set(EntityFields.ExerciseDuration, exerciseDuration);
			
			return this;
		}
		
		public Builder order(Long order) {
			entityBuilder.set(EntityFields.Order, order);
			
			return this;
		}

		public FullEntity<?> build() {
			return entityBuilder.build();
		}
		
	}
}
