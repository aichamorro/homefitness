package com.fitnesse.workouts.repositories.datastore.entities;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.FullEntity;
import com.google.cloud.datastore.IncompleteKey;
import com.google.cloud.datastore.Key;

public final class ExerciseDatastoreEntity {
	
	public static final String Kind = "Exercise";
	
	final public class EntityFields {
		public static final String Name = "name";
		public static final String Duration = "duration";
		public static final String Rest = "rest";
		public static final String WorkoutSet = "workoutSet";
		public static final String Order = "order";
	}
		
	static public String getName(FullEntity<?> entity) {
		return entity.getString(EntityFields.Name);
	}
	
	static public Long getDuration(FullEntity<?> entity) {
		return entity.getLong(EntityFields.Duration);
	}
	
	static public Long getRest(FullEntity<?> entity) {
		return entity.getLong(EntityFields.Rest);
	}

	public static Builder builder(Datastore datastore) {
		return new Builder(datastore.newKeyFactory().kind(Kind).newKey());
	}
	
	public static class Builder {

		private com.google.cloud.datastore.FullEntity.Builder<IncompleteKey> entityBuilder;

		public Builder(IncompleteKey newKey) {
			entityBuilder = Entity.builder(newKey);
		}

		public Builder name(String name) {
			entityBuilder.set(EntityFields.Name, name);
			
			return this;
		}

		public Builder duration(Long duration) {
			entityBuilder.set(EntityFields.Duration, duration);
			
			return this;
		}

		public Builder rest(Long rest) {
			entityBuilder.set(EntityFields.Rest, rest);
			
			return this;
		}
		
		public Builder order(Long order) {
			entityBuilder.set(EntityFields.Order, order);
			
			return this;
		}

		public FullEntity<?> build() {
			return entityBuilder.build();
		}

		public Builder workoutSet(Key workoutSet) {
			entityBuilder.set(EntityFields.WorkoutSet, workoutSet);
			
			return this;
		}
	}
}
