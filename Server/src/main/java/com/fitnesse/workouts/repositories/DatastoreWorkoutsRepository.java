package com.fitnesse.workouts.repositories;

import java.util.Iterator;

import com.fitnesse.common.QueryResultsToIteratorAdapter;
import com.fitnesse.workouts.repositories.datastore.entities.WorkoutDatastoreEntity;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.FullEntity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.Query;

public final class DatastoreWorkoutsRepository implements WorkoutsRepository<Key, FullEntity<?>> {

	private Datastore datastore;
	
	public DatastoreWorkoutsRepository(Datastore datastore) {
		this.datastore = datastore;
	}
	
	@Override
	public Iterator<FullEntity<?>> findAll() {
		Query<Entity> query = Query.entityQueryBuilder().kind(WorkoutDatastoreEntity.Kind).build();

		return new QueryResultsToIteratorAdapter(datastore.run(query));
	}
		
	@Override
	public Entity save(FullEntity<?> workoutDatastore) {
		return datastore.add(workoutDatastore);
	}
}
