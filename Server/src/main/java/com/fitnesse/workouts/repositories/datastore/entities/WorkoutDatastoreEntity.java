package com.fitnesse.workouts.repositories.datastore.entities;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.FullEntity;
import com.google.cloud.datastore.IncompleteKey;
import com.google.cloud.datastore.Key;

public final class WorkoutDatastoreEntity {
	 
	public static final String Kind = "Workout";
	
	private class EntityFields {
		static final String Duration = "totalTime";
		static final String Name = "name";
		static final String ImageUrl = "imageURL";
	}
	
	static public Key getKey(FullEntity<?> entity) {
		return entity instanceof Entity ? ((Entity)entity).key() : null;
	}
	
	static public String getName(FullEntity<?> entity) {
		return entity.getString(EntityFields.Name);
	}
	
	static public Long getDuration(FullEntity<?> entity) {
		return entity.getLong(EntityFields.Duration);
	}
	
	static public String getImageUrl(FullEntity<?> entity) {
		return entity.getString(EntityFields.ImageUrl);
	}
	
	public static Builder builder(Datastore datastore) {
		return new Builder(datastore);
	}
	
	public static class Builder {
		com.google.cloud.datastore.FullEntity.Builder<IncompleteKey> entityBuilder;
		
		public Builder(Datastore datastore) {
			IncompleteKey key = datastore.newKeyFactory().kind(Kind).newKey();
			entityBuilder = Entity.builder(key);
		}
		
		public Builder name(String name) {
			entityBuilder.set(EntityFields.Name, name);
			
			return this;
		}
		
		public Builder duration(Long duration) {
			entityBuilder.set(EntityFields.Duration, duration);
			
			return this;
		}
		
		public Builder imageUrl(String imageUrl) {
			entityBuilder.set(EntityFields.ImageUrl, imageUrl);
			
			return this;
		}
		
		public FullEntity<?> build() {
			return entityBuilder.build();
		}
	}
}
