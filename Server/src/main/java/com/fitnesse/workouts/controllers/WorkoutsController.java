package com.fitnesse.workouts.controllers;

import java.io.IOException;
import java.util.Iterator;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.entity.ContentType;

import com.fitnesse.di.ServiceLocator;
import com.fitnesse.services.WorkoutsService;
import com.fitnesse.workouts.entities.Workout;
import com.fitnesse.workouts.entities.WorkoutJsonAdapter;

public final class WorkoutsController extends HttpServlet {
			
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		WorkoutsService workoutsService = (WorkoutsService) ServiceLocator.sharedInstance().getService(WorkoutsService.class);
		Iterator<Workout<?>> allWorkouts = workoutsService.findAllWorkouts();
		
		response.setContentType(ContentType.APPLICATION_JSON.withCharset("utf-8").toString());
		JsonArray allWorkoutsJson = WorkoutJsonAdapter.jsonWorkoutArray(allWorkouts);
		JsonWriter writer = Json.createWriter(response.getOutputStream());
		writer.writeArray(allWorkoutsJson);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JsonReader jsonReader = Json.createReader(request.getInputStream());		
		JsonObject workoutJson = jsonReader.readObject();
		Workout<?> workout = WorkoutJsonAdapter.workoutFromJson(workoutJson);
		
		WorkoutsService workoutsService = (WorkoutsService) ServiceLocator.sharedInstance().getService(WorkoutsService.class);
		workoutsService.save(workout);
	}
	
}
