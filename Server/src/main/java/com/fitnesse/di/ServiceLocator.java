package com.fitnesse.di;

import java.util.Hashtable;

import com.fitnesse.services.WorkoutServiceImpl;
import com.fitnesse.services.WorkoutsService;
import com.fitnesse.workouts.repositories.DatastoreExercisesRepository;
import com.fitnesse.workouts.repositories.DatastoreWorkoutSetRepository;
import com.fitnesse.workouts.repositories.DatastoreWorkoutsRepository;
import com.fitnesse.workouts.repositories.ExercisesRepository;
import com.fitnesse.workouts.repositories.WorkoutSetRepository;
import com.fitnesse.workouts.repositories.WorkoutsRepository;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.cloud.datastore.FullEntity;
import com.google.cloud.datastore.Key;

public final class ServiceLocator {
	
	private Hashtable<Class<?>, Object> services;

	public static final class InstanceHolder {
		public static final ServiceLocator Instance = new ServiceLocator();
	}
	
	public ServiceLocator() {
		services = new Hashtable<>();
		configureServices();
	}
	
	static public ServiceLocator sharedInstance() {
		return ServiceLocator.InstanceHolder.Instance;
	}
	
	@SuppressWarnings("unchecked")
	public Object getService(Class<?> serviceClass) {
		return services.get(serviceClass);
	}
	
	void configureServices() {
		Datastore datastore = DatastoreOptions.defaultInstance().service();
		services.put(Datastore.class, datastore);

		ExercisesRepository<Key, FullEntity<?>> exercisesRepository = new DatastoreExercisesRepository(datastore);
		services.put(ExercisesRepository.class, exercisesRepository);

		WorkoutsRepository<Key, FullEntity<?>> workoutsRepository = new DatastoreWorkoutsRepository(datastore);
		services.put(WorkoutsRepository.class, workoutsRepository);
		
		WorkoutSetRepository<Key, FullEntity<?>> workoutSetRepository = new DatastoreWorkoutSetRepository(datastore);
		services.put(WorkoutSetRepository.class, workoutsRepository);

		WorkoutServiceImpl workoutsService = new WorkoutServiceImpl();
		workoutsService.setWorkoutRepository(workoutsRepository);
		workoutsService.setExerciseRepository(exercisesRepository);
		workoutsService.setWorkoutRepository(workoutSetRepository);
		workoutsService.setDatastore(datastore);
		
		services.put(WorkoutsService.class, workoutsService);
	}
}
