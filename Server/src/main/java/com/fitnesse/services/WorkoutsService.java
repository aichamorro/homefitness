package com.fitnesse.services;

import java.util.Iterator;

import com.fitnesse.workouts.entities.Workout;

public interface WorkoutsService {
	public Iterator<Workout<?>> findAllWorkouts();
	public Workout<?> save(Workout<?> workout);
}
