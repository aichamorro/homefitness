package com.fitnesse.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fitnesse.common.IteratorAdapter;
import com.fitnesse.common.IteratorUtils;
import com.fitnesse.workouts.entities.Exercise;
import com.fitnesse.workouts.entities.Workout;
import com.fitnesse.workouts.entities.WorkoutSet;
import com.fitnesse.workouts.repositories.ExercisesRepository;
import com.fitnesse.workouts.repositories.WorkoutSetRepository;
import com.fitnesse.workouts.repositories.WorkoutsRepository;
import com.fitnesse.workouts.repositories.datastore.entities.DatastoreEntityAdapter;
import com.fitnesse.workouts.repositories.datastore.entities.ExerciseDatastoreEntity;
import com.fitnesse.workouts.repositories.datastore.entities.WorkoutDatastoreEntity;
import com.fitnesse.workouts.repositories.datastore.entities.WorkoutSetDatastoreEntity;
import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.FullEntity;
import com.google.cloud.datastore.Key;

public final class WorkoutServiceImpl implements WorkoutsService {

	private Datastore datastore;
	private WorkoutsRepository<Key, FullEntity<?>> workoutsRepository;
	private WorkoutSetRepository<Key, FullEntity<?>> workoutSetRepository;
	private ExercisesRepository<Key, FullEntity<?>> exercisesRepository;

	@Override
	public Iterator<Workout<?>> findAllWorkouts() {
		Iterator<FullEntity<?>> allWorkouts = workoutsRepository.findAll();

		return IteratorUtils.iteratorToListWithAdapter(allWorkouts, workoutEntityAdapter()).iterator();
	}

	@Override
	public Workout<?> save(Workout<?> workout) {
		FullEntity<?> workoutDatastore = WorkoutDatastoreEntity.builder(datastore)
													.name(workout.getName())
													.duration(workout.getDuration())
													.imageUrl(workout.getImageUrl())
													.build();
		
		workoutDatastore = workoutsRepository.save(workoutDatastore);
		saveWorkoutSets(workout.getWorkoutSets(), 
						WorkoutDatastoreEntity.getKey(workoutDatastore));
		
		return workout;
	}
	
	private void saveWorkoutSets(List<WorkoutSet> workoutSets, Key workoutId) {
		WorkoutSet workoutSet;

		for (int i=0; i<workoutSets.size(); i++) {
			workoutSet = workoutSets.get(i);
			FullEntity<?> workoutSetDatastoreEntity = WorkoutSetDatastoreEntity.builder(datastore)
					.iterations(workoutSet.getRepetitions())
					.workout(workoutId)
					.rest(workoutSet.getRest())
					.exerciseRest(workoutSet.getExerciseRest())
					.exerciseDuration(workoutSet.getExerciseDuration())
					.order(new Long(i+1))
					.build();
			
			workoutSetDatastoreEntity = workoutSetRepository.save(workoutSetDatastoreEntity);
			saveExercises(workoutSet.getExercises(), WorkoutSetDatastoreEntity.getKey(workoutSetDatastoreEntity));
		}
	}
	
	private void saveExercises(List<Exercise> exercises, Key workoutSetId) {
		ArrayList<FullEntity<?>> entities = new ArrayList<>();

		Exercise exercise;
		FullEntity<?> exerciseDatastoreEntity;
		for (int i=0;i<exercises.size(); i++) {
			exercise = exercises.get(i);
			
			exerciseDatastoreEntity = ExerciseDatastoreEntity.builder(datastore)
					.name(exercise.getName())
					.order(new Long(i+1))
					.workoutSet(workoutSetId)
					.build();
			
			entities.add(exerciseDatastoreEntity);			
		}
		
		FullEntity<?>[] fullEntitiesArray = new FullEntity<?>[entities.size()];
		entities.toArray(fullEntitiesArray);
		exercisesRepository.saveAll(fullEntitiesArray);
	}

	public void setWorkoutRepository(WorkoutsRepository<Key, FullEntity<?>> workoutsRepository) {
		this.workoutsRepository = workoutsRepository;
	}

	public void setExerciseRepository(ExercisesRepository<Key, FullEntity<?>> exercisesRepository) {
		this.exercisesRepository = exercisesRepository;
	}

	public void setWorkoutRepository(WorkoutSetRepository<Key, FullEntity<?>> workoutSetRepository) {
		this.workoutSetRepository = workoutSetRepository;
	}
	
	public void setDatastore(Datastore datastore) {
		this.datastore = datastore;
	}

	// MARK: Internal stuff

	IteratorAdapter<FullEntity<?>, Workout<?>> workoutEntityAdapter() {
		return new IteratorAdapter<FullEntity<?>, Workout<?>>() {
			@Override
			public Workout<?> objectForEntity(FullEntity<?> entity) {
				Iterator<FullEntity<?>> workoutSetIterator = workoutSetRepository
						.findByWorkout(WorkoutDatastoreEntity.getKey(entity));
						
				List<WorkoutSet> workoutSets = IteratorUtils.iteratorToListWithAdapter(workoutSetIterator,
						workoutSetEntityAdapter());

				return DatastoreEntityAdapter.workoutEntityToWorkout(entity, workoutSets);
			}
		};
	}

	IteratorAdapter<FullEntity<?>, WorkoutSet> workoutSetEntityAdapter() {
		return new IteratorAdapter<FullEntity<?>, WorkoutSet>() {
			@Override
			public WorkoutSet objectForEntity(FullEntity<?> entity) {
				Iterator<FullEntity<?>> exercisesIterator = exercisesRepository
						.findByWorkoutSet(WorkoutSetDatastoreEntity.getKey(entity));
				List<Exercise> exerciseList = IteratorUtils.iteratorToListWithAdapter(exercisesIterator,
						exerciseEntityAdapter());

				return DatastoreEntityAdapter.workoutSetEntityToWorkoutSet(entity, exerciseList);
			}
		};
	}

	IteratorAdapter<FullEntity<?>, Exercise> exerciseEntityAdapter() {
		return new IteratorAdapter<FullEntity<?>, Exercise>() {
			@Override
			public Exercise objectForEntity(FullEntity<?> entity) {
				return DatastoreEntityAdapter.exerciseEntityToExercise(entity);
			}
		};

	}
}
