package com.fitnesse.common;

public interface IteratorAdapter<O,N> {
	N objectForEntity(O entity);
}
