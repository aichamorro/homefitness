package com.fitnesse.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class IteratorUtils {
	static public <C, N> List<N> iteratorToListWithAdapter(Iterator<C> iterator,
			IteratorAdapter<C, N> adapter) {
		ArrayList<N> result = new ArrayList<>();

		while (iterator.hasNext()) {
			result.add(adapter.objectForEntity(iterator.next()));
		}

		return result;
	}

}
