package com.fitnesse.common;

import java.util.Iterator;

import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.FullEntity;
import com.google.cloud.datastore.QueryResults;

public final class QueryResultsToIteratorAdapter implements Iterator<FullEntity<?>> {

	private QueryResults<Entity> queryResults;

	public QueryResultsToIteratorAdapter(QueryResults<Entity> queryResults) {
		this.queryResults = queryResults;
	}
	
	@Override
	public boolean hasNext() {
		return queryResults.hasNext();
	}

	@Override
	public FullEntity<?> next() {
		return queryResults.next();
	}

}
