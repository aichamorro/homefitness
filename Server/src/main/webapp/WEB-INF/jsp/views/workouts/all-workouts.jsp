<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
[
  {"name":"Easy Does HIIT",
  "exercises":[
      {"name": "Sprint",
      "duration": "20"},
      {"name": "Rest",
      "duration": "10"},
      {"name": "Squat",
      "duration": "20"},
      {"name": "Rest",
      "duration": "10"},
      {"name": "Jumping Jack",
      "duration": "20"},
      {"name": "Rest",
      "duration": "10"},
      {"name": "Press-Up",
      "duration": "20"},
      {"name": "Rest",
      "duration": "10"}],
  "repeats": "3",
  "interal-rest": "60",
  "imageUrl": "http://greatist.com/sites/default/files/styles/article_main/public/PerfectPushup_BadPosture-Up.jpg?itok=m5j-k6Pg"}
]
